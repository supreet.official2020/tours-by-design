jQuery(document).ready(function ($) {
  //smooth scroll
  $("a.page-scroll").bind("click", function (event) {
    var $anchor = $(this);
    $("html, body")
      .stop()
      .animate({
          scrollTop: $($anchor.attr("href")).offset().top - 120
        },
        1500,
        "easeInOutExpo"
      );
    event.preventDefault();
  });

  $(window).scroll(function () {
    var scroll = $(window).scrollTop();
    var header = $('.header');
    var headerHeight = header.outerHeight();

    if (scroll >= headerHeight) {
      header.addClass('header-fixed');
      if ($('.header-static')[0]) {
        $('body').css({
          paddingTop: headerHeight
        });
      }
    } else {
      header.removeClass('header-fixed');
      $('body').css({
        paddingTop: ''
      });
    }
  });

  //limit characters
  $("p").each(function () {
    "use strict";
    var maxLength = parseInt($(this).attr("data-maxlength"));
    var txt = $(this).text();
    if (txt.length > maxLength)
      $(this).text(txt.substring(0, maxLength) + ".....");
  });

  //carousel init
  $("#carousel1").carousel({
    interval: 3000,
    pause: "false"
  });

  $(".carousel-overlay-title .heading-fancy").addClass(
    "animate__animated animate__fadeInDown"
  );
  $(".carousel-overlay-cont>*").addClass("animate__animated animate__fadeInUp");

  //changing img to wrapped div's background
  imgToBg();

  // if the target of the click isn't the container nor a descendant of the container
  function containerOutClock(target, toAddClass) {
    $(document).mouseup(function (e) {
      var container = $(target);

      if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(target).removeClass(toAddClass);
      }
    });
  }

  //setting overlay outer height
  $(document).ready(squareItem);
  $(window).resize(squareItem);

  function squareItem() {
    $(".square").each(function () {
      "use strict";
      var $this = $(this);
      var widthValue = $this.width();
      $this.css("height", widthValue);
    });
  }

  //wrapping with span
  $(".link-style-passby").wrapInner("<span></span>");

  //copy img to background
  function imgToBg() {
    "use strict";
    var $classForBg = $(".imgtobg-img");
    $classForBg.addClass("imgtobg");
    $(".imgtobg").each(function () {
      "use strict";
      var $this = $(this);
      var thissrc = $(this).attr("src");
      $this.wrap(
        '<div class="imgtobg-o" style="background-image:url(' +
        thissrc +
        ')"></div>'
      );
      $this.hide();
    });

    var $classForBgSm = $(".imgtobg-img-sm");
    $classForBgSm.addClass("imgtobg-sm");
    $(".imgtobg-sm").each(function () {
      "use strict";
      var $this = $(this);
      var thissrc = $(this).attr("src");
      $this.wrap(
        '<div class="imgtobg-o-sm app-xs" style="background-image:url(' +
        thissrc +
        ')"></div>'
      );
      $this.hide();
    });
  }

  $('.tours-card-inner').click(function () {
    $('#toursModalInner').addClass('active');
  });

  $('.tours-card-link').click(function (event) {
    event.preventDefault();

    var link = $(this).attr('href');
    $(link).addClass('active');
    console.log(link);
  });

  $('.tours-modal-exit').click(function () {
    toursModalHide($(this));
  });

  $('.tours-card-link').click(function () {
    toursModalShow();
  })

  $('.tours-modal-exit').click(function () {
    toursModalHide();
  })

  function toursModalShow() {
    $('#toursModal').addClass('active');
    if ($('.tours-single-slider')[0]) {
      $('.tours-single-slider').slick('setPosition');
    }
  }

  function toursModalHide(button) {
    button.parents('.tours-modal').removeClass('active');
  }

  $('.tours-single-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrow: true,
    autoplay: true,
    autoplaySpeed: 7000,
    prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"><polygon points="315.869,21.178 294.621,0 91.566,203.718 294.621,407.436 315.869,386.258 133.924,203.718 "/></svg></button>',
    nextArrow: '<button class="slick-next slick-arrow" aria-label="Previous" type="button"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"><polygon points="112.814,0 91.566,21.178 273.512,203.718 91.566,386.258 112.814,407.436 315.869,203.718 "/></svg></button>',
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 567,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.other-tours-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrow: true,
    autoplay: true,
    autoplaySpeed: 7000,
    prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"><polygon points="315.869,21.178 294.621,0 91.566,203.718 294.621,407.436 315.869,386.258 133.924,203.718 "/></svg></button>',
    nextArrow: '<button class="slick-next slick-arrow" aria-label="Previous" type="button"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"><polygon points="112.814,0 91.566,21.178 273.512,203.718 91.566,386.258 112.814,407.436 315.869,203.718 "/></svg></button>',
    responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 567,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });



  $('.review-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrow: true,
    autoplay: true,
    autoplaySpeed: 7000,
    prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"><polygon points="315.869,21.178 294.621,0 91.566,203.718 294.621,407.436 315.869,386.258 133.924,203.718 "/></svg></button>',
    nextArrow: '<button class="slick-next slick-arrow" aria-label="Previous" type="button"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"><polygon points="112.814,0 91.566,21.178 273.512,203.718 91.566,386.258 112.814,407.436 315.869,203.718 "/></svg></button>',
  });

  $('.blog-card-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    dots: true,
    fade: true,
    arrow: true,
    autoplay: false,
    prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"><polygon points="315.869,21.178 294.621,0 91.566,203.718 294.621,407.436 315.869,386.258 133.924,203.718 "/></svg></button>',
    nextArrow: '<button class="slick-next slick-arrow" aria-label="Previous" type="button"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 407.436 407.436" style="enable-background:new 0 0 407.436 407.436;" xml:space="preserve"><polygon points="112.814,0 91.566,21.178 273.512,203.718 91.566,386.258 112.814,407.436 315.869,203.718 "/></svg></button>',
  });

  $('.blog-card-slider').prepend('<div class="slick-slider-nav"></div>')
  $('.blog-card-slider .slick-arrow,.blog-card-slider .slick-dots').prependTo('.blog-card-slider .slick-slider-nav')

  // sticky init
  $(".sticky-div").stick_in_parent({
    offset_top: 100,
    offset_bottom: 100
  });

  $('.showmore-btn').click(function () {
    var wrapper = $(this).parents('.tours-single-itenary');
    var wrapperHeight = wrapper.outerHeight();
    var content = wrapper.find('.tours-single-itenary-content');
    var btn = $(this);
    wrapper.css('height', wrapperHeight);

    content.css({
      height: 'auto'
    });


    if ($(content).hasClass('maximized')) {
      wrapper.css('height', 'auto');

      content.animate({
        height: 0
      }, 1000, function () {
        content.removeClass('maximized');
        $(btn).text('Show More');
      })
    } else {
      var contentHeight = content.outerHeight();
      content.addClass('maximized');

      wrapper.animate({
        height: contentHeight
      }, 1000, function () {
        $(btn).text('Show Less');
      });
    }



  });

  $('.itenary-accordion-item-header').click(function () {
    var item = $(this).parents('.itenary-accordion-item');
    var itemBody = item.find('.itenary-accordion-item-body');

    if ($(item).hasClass('active')) {
      item.removeClass('active');
      itemBody.slideUp('fast');
    } else {
      item.addClass('active');
      itemBody.slideDown('fast');
    }
  });

  $('#dateRangePicker').daterangepicker({
    "timePicker": true,
    "startDate": new Date(),
  }, function (start, end, label) {
    console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
  });
  $('#datePicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: new Date(),
  });

  function kidsNumField() {
    var $this = $('#kidsNum'),
      fieldValue = $this.val(),
      parentDiv = $this.parents('form').find('.kids-agegroup'),
      parentDivRow = parentDiv.find('.row');

    parentDivRow.empty();
    if (fieldValue > 0) {
      for (i = 0; i < fieldValue; i++) {
        parentDivRow.append('<div class="col-lg-3"><label>Kid #' + (i + 1) + '</label><select name="kidsNumAge[]" class="form-control"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option><option>11</option><option>12</option><option>13</option><option>14</option><option>15</option><option>16</option><option>17</option></select></div>')
      }
      parentDiv.slideDown();
    } else {
      parentDiv.slideUp();
    }
  }

  kidsNumField();
  $('#kidsNum').on('change', function () {
    kidsNumField();
  });

  $('.nav-toggle').click(function () {
    navToggle()
  });

  function navToggle() {
    var $this = $('.nav-toggle');
    var headerNav = $('.header-navigation');

    if (headerNav.hasClass('open')) {
      $this.removeClass('open');
      headerNav.removeClass('open');
      $('body').css('overflowY', '');
    } else {
      $this.addClass('open');
      headerNav.addClass('open');
      $('body').css('overflowY', 'hidden');
    }
  }

  $('#enquireForm .heading-fancy').click(function () {
    var parent = $(this).parents('#enquireForm');
    var target = parent.find('.tours-single-form');
    if ((target).is(':visible')) {
      console.log('yeah');
      target.slideUp();
      $(this).removeClass('toggledOpen');
    } else {
      target.slideDown();
      $(this).addClass('toggledOpen');
    }
  });

  $('.header-searchbtn').click(function () {
    $('.search-popup').addClass('open');
    $('.search-popup input[type="text"]')[0].focus();
  });

  $('.search-popup button[type="reset"]').click(function () {
    $('.search-popup').removeClass('open');
  });

  $(document).keyup(function (e) {
    if (e.keyCode === 27) {
      $('.search-popup').removeClass('open');
      $('.search-popup input[type="text"]')[0].focusout();
    }
  });

  $(document).mouseup(function (e) {
    var container = $('.search-popup form');
    if ($('.search-popup').hasClass('open')) {
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        $('.search-popup').removeClass('open');
      }
    }
  });
});